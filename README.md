**This project is archived because it is my first project created a long time ago and with poor quality code. I decided to keep it just for an emotional bond.**

# SenderMTP ![Mail GIF](http://www.giorgiotave.it/wp-content/gallery/e-mail/A_10mailput.gif)
**S**ender**MTP** is a tool to send "anonymous" e-mails and be identified like other e-mails addresses. It use the [telnetlib](https://docs.python.org/3.5/library/telnetlib.html) library to connect over telnet at the desired SMTP server.

**- Downloads, Execution & facoltative Installation**

`git clone https://github.com/emorraa/SenderMTP`

`cd SenderMTP/`

`python3 SenderMTP.py`

**facoltative installation ->**

`sudo pip install pyinstaller` <-- Download [PyInstaller](pyinstaller.org)

`pyinstaller SenderMTP.py`     <-- Create an executable

**- Example of the initial connection**

![alt text](https://raw.githubusercontent.com/emorraa/SenderMTP/master/example_screen.png)

**- Thanks**

A special thanks to my **epystolary brother**... :eagle:



 **The author of this tool does not assume any responsibility for any use that you make!**
