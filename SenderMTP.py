#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import telnetlib

#BANNER
print("\n\033[91m _____              _            _____  _____  _____ ")
print("|   __| ___  ___  _| | ___  ___ |     ||_   _||  _  |")
print("|__   || -_||   || . || -_||  _|| | | |  | |  |   __|")
print("|_____||___||_|_||___||___||_|  |_|_|_|  |_|  |__|   ")
print("______________________________________________________by Phreaker\033[0m")

#CONNECTION & HELO
connect_server = input("\nMail SMTP server to connect: ")
connect_port = int(eval(input("Port: ")))
tn = telnetlib.Telnet(connect_server, connect_port)
tn.open(connect_server, connect_port)
helo = "HELO "+ connect_server+ "\r\n"
tn.write(helo.encode("ascii"))

#TLS SUPPORT
select_STARTTLS = input("Do you want to use TLS ('yes' or 'no'): ")
if select_STARTTLS == "yes":
	STARTTLS = "STARTTLS\r\n"

#TLS ERROR
	STARTTLS_error = "500"
	tn.write(STARTTLS.encode("ascii"))
	tn.read_until(STARTTLS_error.encode("ascii"))
	print("\nYour mail server does not support \"TLS\". Restart SenderMTP without activating the \"TLS\" with \"yes\" parameter")
	exit()

#MAIL FROM
print("\n        -- Mail from -- \n")
mailFrom_input = input("Insert E-mail: ")
mailFrom_input =  mailFrom_input.rstrip(os.linesep)
mailFrom = "MAIL FROM:<"+ mailFrom_input+ ">\n"
tn.write(mailFrom.encode("ascii"))

#RCPT TO
print("\n          -- Mail to -- \n")
mailRcptTo_input = input("Insert E-mail: ")
mailRcptTo_input =  mailRcptTo_input.rstrip(os.linesep)
mailRcptTo = "RCPT TO:<"+ mailRcptTo_input+ ">\n"
tn.write(mailRcptTo.encode("ascii"))

#MAIL INPUT AND DETAILS INSERTION
print("\n   -- Mail details -- \n")
DATA_command = "DATA"+ "\n"
tn.write(DATA_command.encode("ascii"))
data_date = input("Date: ")
data_from = input("From who: ")
data_subject = input("Subject: ")
data_body = input("Body: ")

#MAIL INPUT AND DETAILS SENDING
data = "Date: "+ data_date+ "\r\n"+ "From: "+ data_from+ "\r\n"+ "Subject: "+ data_subject+ "\r\n"+ "\n"+ data_body+ "\r\n"+ ".\r\n" 
tn.write(data.encode("ascii"))

#MAIL SENDED CONFIRMATION
mailSended = "Mail accepted"
tn.read_until(mailSended.encode("ascii"))
print("")
print("Mail successfully sended to " + mailRcptTo_input)